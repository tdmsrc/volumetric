#!/bin/sh

#run from /tools folder; parent contains /src
#requires node package uglifyjs

cd ../src

echo "Assembling GLSL resources"
nodejs ../tools/staticrc.js \
inclDataDims.txt glsl/inclDataDims.txt \
inclDataOctree.txt glsl/inclDataOctree.txt \
inclDataVoxels.txt glsl/inclDataVoxels.txt \
inclLighting.txt glsl/inclLighting.txt \
inclPackColor.txt glsl/inclPackColor.txt \
inclPackFloat.txt glsl/inclPackFloat.txt \
quadVertCopy.txt glsl/quadVertCopy.txt \
quadFragCopy.txt glsl/quadFragCopy.txt \
quadFragDebugData.txt glsl/quadFragDebugData.txt \
quadFragDebugOctree.txt glsl/quadFragDebugOctree.txt \
quadFragBlurH.txt glsl/quadFragBlurH.txt \
quadFragBlurV.txt glsl/quadFragBlurV.txt \
quadFragData.txt glsl/quadFragData.txt \
quadFragNormals.txt glsl/quadFragNormals.txt \
quadFragOctree.txt glsl/quadFragOctree.txt \
quadFragOctreeSub.txt glsl/quadFragOctreeSub.txt \
quadFragPP.txt glsl/quadFragPP.txt \
quadFragDOF.txt glsl/quadFragDOF.txt \
quadFragSSAO.txt glsl/quadFragSSAO.txt \
quadVertRender.txt glsl/quadVertRender.txt \
quadFragRender.txt glsl/quadFragRender.txt \
-o glsl/glslrc.js

echo "Minifying JS: volumetric.min.js"
$(npm bin)/uglifyjs \
glsl/glslrc.js \
js/voxShaderQuad.js \
-m -o jsmin/volumetric.min.js

