precision highp float;

uniform sampler2D texColor;
varying vec2 texCoord;


void main()
{
  //SEE PACKING
  vec4 rgba = texture2D(texColor, texCoord);
  
  //read "l"th bit in A
  float val = floor(rgba.a == 1.0 ? 255.0 : rgba.a * 256.0);
  
  float b0 = mod(val, 2.0); val = floor(val / 2.0);
  float b1 = mod(val, 2.0); val = floor(val / 2.0);
  float b2 = mod(val, 2.0); val = floor(val / 2.0);
  float b3 = mod(val, 2.0); val = floor(val / 2.0);
  
  float b4 = mod(val, 2.0); val = floor(val / 2.0);
  float b5 = mod(val, 2.0); val = floor(val / 2.0);
  float b6 = mod(val, 2.0); val = floor(val / 2.0);
  float b7 = mod(val, 2.0);
  
  vec3 color = vec3(0.0, 0.0, 0.0);
  if(b0 == 1.0){ color = vec3(1.0, 1.0, 1.0); }
  if(b1 == 1.0){ color = vec3(1.0, 1.0, 0.0); }
  if(b2 == 1.0){ color += vec3(0.5, 0.0, 0.0); }
  if(b3 == 1.0){ color += vec3(0.25, 0.0, 0.0); }
  if(b4 == 1.0){ color += vec3(0.0, 0.5, 0.0); }
  if(b5 == 1.0){ color += vec3(0.0, 0.25, 0.0); }
  if(b6 == 1.0){ color += vec3(0.0, 0.0, 0.5); }
  if(b7 == 1.0){ color += vec3(0.0, 0.0, 0.25); }
  
  gl_FragColor = vec4(color, 1.0);
}
