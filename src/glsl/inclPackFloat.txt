
//----------------------------------------------------
//PACKING
//methods for packing and unpacking float/2 bytes
//----------------------------------------------------

//pack: float in [0,1] -> 2 bytes in [0,1]
vec2 packFloat(float d){
  float dpack = d * 255.0;
  
  float dpackx = floor(dpack) / 255.0;
  float dpacky = fract(dpack);
  
  return vec2(dpackx, dpacky);
}

//unpack: 2 bytes in [0,1] -> float in [0,1]
float unpackFloat(vec2 v){
  return (floor(v.x * 255.0) + v.y) / 255.0;
}

