//creates: WGLShaderQuad[XYZ]

(function(_NS, undefined){

  //import
  var WGLShaderProgram = _NS.WGLShaderProgram;
  
  //===================================
  // SHADER PROGRAM: COMMON
  //===================================
  
  /** Shader program for use with {@link WGLQuad}.
  * @constructs WGLShaderQuadCommon
  * @extends WGLShaderProgram
  */
  var WGLShaderQuadCommon = function(){ }
  WGLShaderQuadCommon.prototype = new WGLShaderProgram();
  
  //define and set vertex attributes
  //-----------------------------------
  var VERTEX_POSITION_ATTRIBUTE = 0;
  var VERTEX_TEX_ATTRIBUTE = 1;
  
  WGLShaderQuadCommon.prototype._setVertexAttributes = function(gl){
    this._addVertexAttribute(gl, VERTEX_POSITION_ATTRIBUTE, "vertexPosition");
    this._addVertexAttribute(gl, VERTEX_TEX_ATTRIBUTE, "vertexTex");
  };
  
  /** Specify a quad's vertex attributes.  Use this before {@link WGLQuad#draw}.
  * @param {WebGLRenderingContext} gl
  * @param {WGLQuad} quad
  */
  WGLShaderQuadCommon.prototype.prepareVertexAttribs = function(gl, quad){
    gl.bindBuffer(gl.ARRAY_BUFFER, quad.bufPosition);
    gl.vertexAttribPointer(VERTEX_POSITION_ATTRIBUTE, 2, gl.FLOAT, false, 0, 0);
    
    gl.bindBuffer(gl.ARRAY_BUFFER, quad.bufTex);
    gl.vertexAttribPointer(VERTEX_TEX_ATTRIBUTE, 2, gl.FLOAT, false, 0, 0);
  };
  
  
  //===================================
  // SHADER PROGRAM: QUAD COPY
  //===================================
  
  /** See {@link WGLShaderProgram#_initialize}
  * @constructs WGLShaderQuadCopy
  * @extends WGLShaderQuadCommon
  */
  var WGLShaderQuadCopy = function(gl, description, shaderVert, shaderFrag){ 
    this._initialize(gl, description, shaderVert, shaderFrag);
  };
  WGLShaderQuadCopy.prototype = new WGLShaderQuadCommon();
  
  //export
  _NS.WGLShaderQuadCopy = WGLShaderQuadCopy;
  
  //define and set uniforms
  //-----------------------------------
  //define texture units
  WGLShaderQuadCopy.COLOR_TEXTURE_UNIT = 0;
  
  WGLShaderQuadCopy.prototype._getUniformNames = function(uniformNames){
    uniformNames.push("texColor");
  };
  
  WGLShaderQuadCopy.prototype.use = function(gl){
    WGLShaderProgram.prototype.use.call(this, gl);
    
    this.setU1i(gl, "texColor", WGLShaderQuadCopy.COLOR_TEXTURE_UNIT);
  };
  
  
  //===================================
  // SHADER PROGRAM: QUAD BLUR
  //===================================
  
  /** See {@link WGLShaderProgram#_initialize}
  * @constructs WGLShaderQuadBlur
  * @extends WGLShaderQuadCommon
  */
  var WGLShaderQuadBlur = function(gl, description, shaderVert, shaderFrag){ 
    this._initialize(gl, description, shaderVert, shaderFrag);
  };
  WGLShaderQuadBlur.prototype = new WGLShaderQuadCommon();
  
  //export
  _NS.WGLShaderQuadBlur = WGLShaderQuadBlur;
  
  //define and set uniforms
  //-----------------------------------
  //define texture units
  WGLShaderQuadBlur.COLOR_TEXTURE_UNIT = 0;
  
  WGLShaderQuadBlur.prototype._getUniformNames = function(uniformNames){
    uniformNames.push("texColor", "texColorSize");
  };
  
  WGLShaderQuadBlur.prototype.use = function(gl){
    WGLShaderProgram.prototype.use.call(this, gl);
    
    this.setU1i(gl, "texColor", WGLShaderQuadBlur.COLOR_TEXTURE_UNIT);
  };
  
  /** Set uniforms associated with the source texture's pixel size.
  * @param {WebGLRenderingContext} gl
  * @param {number} texWidth
  * @param {number} texHeight
  */
  WGLShaderQuadBlur.prototype.setSourceTextureSize = function(gl, texWidth, texHeight){
    this.setU2i(gl, "texColorSize", texWidth, texHeight);
  };
  
  
  //===================================
  // SHADER PROGRAM: POST-PROCESSING
  //===================================
  
  /** See {@link WGLShaderProgram#_initialize}
  * @constructs WGLShaderQuadPP
  * @extends WGLShaderQuadCommon
  */
  var WGLShaderQuadPP = function(gl, description, shaderVert, shaderFrag){ 
    this._initialize(gl, description, shaderVert, shaderFrag);
  };
  WGLShaderQuadPP.prototype = new WGLShaderQuadCommon();
  
  //export
  _NS.WGLShaderQuadPP = WGLShaderQuadPP;
  
  //define and set uniforms
  //-----------------------------------
  //define texture units
  WGLShaderQuadPP.COLOR_TEXTURE_UNIT = 0;
  WGLShaderQuadPP.SSAO_TEXTURE_UNIT = 1;
  
  WGLShaderQuadPP.prototype._getUniformNames = function(uniformNames){
    uniformNames.push(
      "texColor", "texSSAO", "modeDebugSSAO",
      "fogMin", "fogMax", "fogColor",
      "colorAdjustBrightness", "colorAdjustContrast", "colorAdjustSaturation"
    );
  };
  
  WGLShaderQuadPP.prototype.use = function(gl){
    WGLShaderProgram.prototype.use.call(this, gl);
    
    this.setU1i(gl, "texColor", WGLShaderQuadPP.COLOR_TEXTURE_UNIT);
    this.setU1i(gl, "texSSAO", WGLShaderQuadPP.SSAO_TEXTURE_UNIT);
  };
  
  /** Set uniforms for setting render mode.
  * Object should have properties: modeDebugSSAO.
  * @param {WebGLRenderingContext} gl
  * @param {Object} object
  */
  WGLShaderQuadPP.prototype.setRenderMode = function(gl, object){
    this.setU1i(gl, "modeDebugSSAO", object.modeDebugSSAO ? 1 : 0);
  };
  
  /** Set uniforms for fog parameters.
  * Object should have properties: fogMin, fogMax, fogColor.
  * @param {WebGLRenderingContext} gl
  * @param {Object} object
  */
  WGLShaderQuadPP.prototype.setFogUniforms = function(gl, object){
    this.setU1f(gl, "fogMin", object.fogMin);
    this.setU1f(gl, "fogMax", object.fogMax);
    this.setUVec3(gl, "fogColor", object.fogColor);
  };
  
  /** Set uniforms for adjusting color.
  * Object should have properties: adjustBrightness, adjustContrast, adjustSaturation.
  * @param {WebGLRenderingContext} gl
  * @param {Object} object
  */
  WGLShaderQuadPP.prototype.setColorAdjustUniforms = function(gl, object){
    this.setU1f(gl, "colorAdjustBrightness", object.adjustBrightness);
    this.setU1f(gl, "colorAdjustContrast", object.adjustContrast);
    this.setU1f(gl, "colorAdjustSaturation", object.adjustSaturation);
  };
  
  
  //===================================
  // SHADER PROGRAM: DEPTH OF FIELD
  //===================================
  
  /** See {@link WGLShaderProgram#_initialize}
  * @constructs WGLShaderQuadDOF
  * @extends WGLShaderQuadCommon
  */
  var WGLShaderQuadDOF = function(gl, description, shaderVert, shaderFrag){ 
    this._initialize(gl, description, shaderVert, shaderFrag);
  };
  WGLShaderQuadDOF.prototype = new WGLShaderQuadCommon();
  
  //export
  _NS.WGLShaderQuadDOF = WGLShaderQuadDOF;
  
  //define and set uniforms
  //-----------------------------------
  //define texture units
  WGLShaderQuadDOF.COLOR_TEXTURE_UNIT = 0;
  WGLShaderQuadDOF.BLUR_TEXTURE_UNIT = 1;
  
  WGLShaderQuadDOF.prototype._getUniformNames = function(uniformNames){
    uniformNames.push(
      "texColor", "texBlur",
      "blurMin", "blurMax"
    );
  };
  
  WGLShaderQuadDOF.prototype.use = function(gl){
    WGLShaderProgram.prototype.use.call(this, gl);
    
    this.setU1i(gl, "texColor", WGLShaderQuadDOF.COLOR_TEXTURE_UNIT);
    this.setU1i(gl, "texBlur", WGLShaderQuadDOF.BLUR_TEXTURE_UNIT);
  };
  
  /** Set uniforms for blur parameters.
  * Object should have properties: blurMin, blurMax.
  * @param {WebGLRenderingContext} gl
  * @param {Object} object
  */
  WGLShaderQuadDOF.prototype.setBlurUniforms = function(gl, object){
    this.setU1f(gl, "blurMin", object.blurMin);
    this.setU1f(gl, "blurMax", object.blurMax);
  };
  
  
  //===================================
  // SHADER PROGRAM: SSAO
  //===================================
  
  /** See {@link WGLShaderProgram#_initialize}
  * @constructs WGLShaderQuadSSAO
  * @extends WGLShaderQuadCommon
  */
  var WGLShaderQuadSSAO = function(gl, description, shaderVert, shaderFrag){ 
    this._initialize(gl, description, shaderVert, shaderFrag);
  };
  WGLShaderQuadSSAO.prototype = new WGLShaderQuadCommon();
  
  //export
  _NS.WGLShaderQuadSSAO = WGLShaderQuadSSAO;
  
  //define and set uniforms
  //-----------------------------------
  //define texture units
  WGLShaderQuadSSAO.COLOR_TEXTURE_UNIT = 0;
  
  WGLShaderQuadSSAO.prototype._getUniformNames = function(uniformNames){
    uniformNames.push(
      "texColor", "texColorSize"
    );
  };
  
  WGLShaderQuadSSAO.prototype.use = function(gl){
    WGLShaderProgram.prototype.use.call(this, gl);
    
    this.setU1i(gl, "texColor", WGLShaderQuadSSAO.COLOR_TEXTURE_UNIT);
  };
  
  /** Set uniforms associated with the source texture's pixel size.
  * @param {WebGLRenderingContext} gl
  * @param {number} texWidth
  * @param {number} texHeight
  */
  WGLShaderQuadSSAO.prototype.setSourceTextureSize = function(gl, texWidth, texHeight){
    this.setU2i(gl, "texColorSize", texWidth, texHeight);
  };
  
  
  //===================================
  // SHADER PROGRAM: DATA TO QUAD
  //===================================
  
  /** See {@link WGLShaderProgram#_initialize}
  * @constructs WGLShaderQuadData
  * @extends WGLShaderQuadCommon
  */
  var WGLShaderQuadData = function(gl, description, shaderVert, shaderFrag){ 
    this._initialize(gl, description, shaderVert, shaderFrag);
  };
  WGLShaderQuadData.prototype = new WGLShaderQuadCommon();
  
  //export
  _NS.WGLShaderQuadData = WGLShaderQuadData;
  
  //define and set uniforms
  //-----------------------------------
  WGLShaderQuadData.prototype._getUniformNames = function(uniformNames){ 
    uniformNames.push("TIME");
  };
  
  /** Set time uniform
  * @param {WebGLRenderingContext} gl
  * @param {number} TIME
  */
  WGLShaderQuadData.prototype.setTIME = function(gl, TIME){
    this.setU1f(gl, "TIME", TIME);
  };
  
  
  //===================================
  // SHADER PROGRAM: NORMALS TO QUAD
  //===================================
  
  /** See {@link WGLShaderProgram#_initialize}
  * @constructs WGLShaderQuadNormals
  * @extends WGLShaderQuadCommon
  */
  var WGLShaderQuadNormals = function(gl, description, shaderVert, shaderFrag){ 
    this._initialize(gl, description, shaderVert, shaderFrag);
  };
  WGLShaderQuadNormals.prototype = new WGLShaderQuadCommon();
  
  //export
  _NS.WGLShaderQuadNormals = WGLShaderQuadNormals;
  
  //define and set uniforms
  //-----------------------------------
  //define texture units
  WGLShaderQuadNormals.VOXEL_TEXTURE_UNIT = 0;
  
  WGLShaderQuadNormals.prototype._getUniformNames = function(uniformNames){
    uniformNames.push("texVoxels", "isoval");
  };
  
  WGLShaderQuadNormals.prototype.use = function(gl){
    WGLShaderProgram.prototype.use.call(this, gl);
    
    this.setU1i(gl, "texVoxels", WGLShaderQuadNormals.VOXEL_TEXTURE_UNIT);
  };
  
  //TODO doc
  WGLShaderQuadNormals.prototype.setIsoVal = function(gl, val){
    this.setU1f(gl, "isoval", val);
  };
  
  
  //===================================
  // SHADER PROGRAM: INITIAL OCTREE TO QUAD
  //===================================
  
  /** See {@link WGLShaderProgram#_initialize}
  * @constructs WGLShaderQuadOctree
  * @extends WGLShaderQuadCommon
  */
  var WGLShaderQuadOctree = function(gl, description, shaderVert, shaderFrag){ 
    this._initialize(gl, description, shaderVert, shaderFrag);
  };
  WGLShaderQuadOctree.prototype = new WGLShaderQuadCommon();
  
  //export
  _NS.WGLShaderQuadOctree = WGLShaderQuadOctree;
  
  //define and set uniforms
  //-----------------------------------  
  //define texture units
  WGLShaderQuadOctree.VOXEL_TEXTURE_UNIT = 0;
  
  WGLShaderQuadOctree.prototype._getUniformNames = function(uniformNames){
    uniformNames.push("texVoxels", "isoval");
  };
  
  WGLShaderQuadOctree.prototype.use = function(gl){
    WGLShaderProgram.prototype.use.call(this, gl);
    
    this.setU1i(gl, "texVoxels", WGLShaderQuadOctree.VOXEL_TEXTURE_UNIT);
  };
  
  //TODO doc
  WGLShaderQuadOctree.prototype.setIsoVal = function(gl, val){
    this.setU1f(gl, "isoval", val);
  };
  
  
  //===================================
  // SHADER PROGRAM: SUB-OCTREE TO QUAD
  //===================================
  
  /** See {@link WGLShaderProgram#_initialize}
  * @constructs WGLShaderQuadOctreeSub
  * @extends WGLShaderQuadCommon
  */
  var WGLShaderQuadOctreeSub = function(gl, description, shaderVert, shaderFrag){ 
    this._initialize(gl, description, shaderVert, shaderFrag);
  };
  WGLShaderQuadOctreeSub.prototype = new WGLShaderQuadCommon();
  
  //export
  _NS.WGLShaderQuadOctreeSub = WGLShaderQuadOctreeSub;
  
  //define and set uniforms
  //-----------------------------------
  //define texture units
  WGLShaderQuadOctreeSub.OCTREE_TEXTURE_UNIT = 1;
  
  WGLShaderQuadOctreeSub.prototype._getUniformNames = function(uniformNames){
    uniformNames.push("texOctree", "level");
  };
  
  WGLShaderQuadOctreeSub.prototype.use = function(gl){
    WGLShaderProgram.prototype.use.call(this, gl);
    
    this.setU1i(gl, "texOctree", WGLShaderQuadOctreeSub.OCTREE_TEXTURE_UNIT);
  };
  
  /** Specify the level of octree to write, from 1 to 7.
  * <ul>
  * <li>level 0 -> [64]^3 (finest level, written by {@link WGLShaderQuadOctree})
  * <li>level 1 -> [32]^3
  * <li>level 2 -> [16]^3 ... etc up to level 7, 1x1x1
  * </ul>
  * @param {WebGLRenderingContext} gl
  * @param {number} level
  */
  WGLShaderQuadOctreeSub.prototype.setLevel = function(gl, level){
    this.setU1f(gl, "level", level);
  };
  
  
  //===================================
  // SHADER PROGRAM: RENDER TO QUAD
  //===================================
  
  /** See {@link WGLShaderProgram#_initialize}
  * @constructs WGLShaderQuadRender
  * @extends WGLShaderQuadCommon
  */
  var WGLShaderQuadRender = function(gl, description, shaderVert, shaderFrag){ 
    this._initialize(gl, description, shaderVert, shaderFrag);
  };
  WGLShaderQuadRender.prototype = new WGLShaderQuadCommon();
  
  //export
  _NS.WGLShaderQuadRender = WGLShaderQuadRender;
  
  //define and set uniforms
  //-----------------------------------
  //define texture units
  WGLShaderQuadRender.VOXEL_TEXTURE_UNIT = 0;
  WGLShaderQuadRender.OCTREE_TEXTURE_UNIT = 1;
  WGLShaderQuadRender.ENV_TEXTURE_UNIT = 2;
  
  WGLShaderQuadRender.prototype._getUniformNames = function(uniformNames){
    uniformNames.push(
      "texVoxels", "texOctree", "texEnv", "texTargetSize",
      "camPosition", "camRotation", "camAspect", "camTanFOVy",
      "lightPosition", "lightColor", "lightAttenuation",
      "ambientCoefficient", "diffuseCoefficient", "specularCoefficient", "specularExponent",
      "reflectionFactor", "skyboxTextureAmount",
      "modeDebug", "modeInterpolate", "isoval"
    );
  };
  
  WGLShaderQuadRender.prototype.use = function(gl){
    WGLShaderProgram.prototype.use.call(this, gl);
    
    this.setU1i(gl, "texVoxels",  WGLShaderQuadRender.VOXEL_TEXTURE_UNIT);
    this.setU1i(gl, "texOctree",  WGLShaderQuadRender.OCTREE_TEXTURE_UNIT);
    this.setU1i(gl, "texEnv",     WGLShaderQuadRender.ENV_TEXTURE_UNIT);
  };
  
  /** Set uniforms associated with the target texture's pixel size.
  * @param {WebGLRenderingContext} gl
  * @param {number} texWidth
  * @param {number} texHeight
  */
  WGLShaderQuadRender.prototype.setTargetTextureSize = function(gl, texWidth, texHeight){
    this.setU2i(gl, "texTargetSize", texWidth, texHeight);
  };
  
  /** Set uniforms associated with a {@link Camera}.
  * @param {WebGLRenderingContext} gl
  * @param {Camera} camera
  */
  WGLShaderQuadRender.prototype.setCameraUniforms = function(gl, camera){
    this.setUVec3(gl, "camPosition", camera.position);
    this.setUMat3(gl, "camRotation", camera.rotation.matrix);
    this.setU1f(gl, "camAspect", camera.aspect);
    this.setU1f(gl, "camTanFOVy", 0.5*Math.tan(camera.fovy*0.017453292519943));
  };
  
  /** Set uniforms associated with a {@link Light}.
  * @param {WebGLRenderingContext} gl
  * @param {Light} light
  */
  WGLShaderQuadRender.prototype.setLightUniforms = function(gl, light){
    this.setUVec3(gl, "lightPosition", light.camera.position);
    this.setUVec3(gl, "lightColor", light.color);
    this.setUVec3(gl, "lightAttenuation", light.attenuation);
  };
  
  /** Set uniforms associated with a material.
  * Object should have properties: ambientCoefficient, diffuseCoefficient, specularCoefficient, specularExponent.
  * @param {WebGLRenderingContext} gl
  * @param {Object} object
  */
  WGLShaderQuadRender.prototype.setMaterialUniforms = function(gl, object){
    this.setU1f(gl, "ambientCoefficient", object.ambientCoefficient);
    this.setU1f(gl, "diffuseCoefficient", object.diffuseCoefficient);
    this.setU1f(gl, "specularCoefficient", object.specularCoefficient);
    this.setU1f(gl, "specularExponent", object.specularExponent);
  };
  
  /** Set uniforms for adjusting material reflection factor.
  * @param {WebGLRenderingContext} gl
  * @param {number} val
  */
  WGLShaderQuadRender.prototype.setMaterialReflection = function(gl, val){
    this.setU1f(gl, "reflectionFactor", val);
  };
  
  /** Set uniforms for adjusting skybox texture factor.
  * @param {WebGLRenderingContext} gl
  * @param {number} val
  */
  WGLShaderQuadRender.prototype.setSkyboxFactor = function(gl, val){
    this.setU1f(gl, "skyboxTextureAmount", val);
  };
  
  /** Set uniforms for setting render mode.
  * Object should have properties: modeDebug, modeInterpolate.
  * @param {WebGLRenderingContext} gl
  * @param {Object} object
  */
  WGLShaderQuadRender.prototype.setRenderMode = function(gl, object){
    this.setU1i(gl, "modeDebug", object.modeDebug ? 1 : 0);
    this.setU1i(gl, "modeInterpolate", object.modeInterpolate ? 1 : 0);
  };
  
  //TODO doc
  WGLShaderQuadRender.prototype.setIsoVal = function(gl, val){
    this.setU1f(gl, "isoval", val);
  };
    
  
})(window.Fig = window.Fig || {});

