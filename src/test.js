var _NS = window.Fig;

var Light = _NS.Light;
var WGLQuad = _NS.WGLQuad;

var WGLShaderQuadCopy =       _NS.WGLShaderQuadCopy;
var WGLShaderQuadBlur =       _NS.WGLShaderQuadBlur;
var WGLShaderQuadPP =         _NS.WGLShaderQuadPP;
var WGLShaderQuadDOF =        _NS.WGLShaderQuadDOF;
var WGLShaderQuadSSAO =       _NS.WGLShaderQuadSSAO;
var WGLShaderQuadData =       _NS.WGLShaderQuadData;
var WGLShaderQuadNormals =    _NS.WGLShaderQuadNormals;
var WGLShaderQuadOctree =     _NS.WGLShaderQuadOctree;
var WGLShaderQuadOctreeSub =  _NS.WGLShaderQuadOctreeSub;
var WGLShaderQuadRender =     _NS.WGLShaderQuadRender;

var WGLFramebufferObject = _NS.WGLFramebufferObject;
var WGLTexture = _NS.WGLTexture;
var WGLTextureCube = _NS.WGLTextureCube;


//get gl
var initgl = function(canvas){
  var opts = { 
    premultipliedAlpha: true, //is default, but make sure
    preserveDrawingBuffer: true //allow toDataURL (snapshots), etc
  };
  try{ gl = canvas.getContext("webgl",opts) || canvas.getContext("experimental-webgl",opts); }
  catch(e){}
  
  if(!gl){
    console.error("Unable to obtain WebGLRenderingContext!");
    return null;
  }
  
  return gl;
};

var $canvas = $("#canvas");
var gl = initgl($canvas[0]);


//create FBO, texture(s), quad, shaders
var fbo = new WGLFramebufferObject(gl);

//TODO
//value here must correspond to values in inclDataDims
var qd = 4096; //256^3 voxels
//var qd = 512; //64^3 voxels
var texData =     new WGLTexture(gl, qd, qd, false);
var texOctreeA =  new WGLTexture(gl, qd, qd, false);
var texOctreeB =  new WGLTexture(gl, qd, qd, false);

var texCube = new WGLTextureCube(gl, 512, 512, true);
texCube.setUP(gl, "img/interstellar_up512.jpg");
texCube.setDN(gl, "img/interstellar_dn512.jpg");
texCube.setFT(gl, "img/interstellar_bk512.jpg");
texCube.setBK(gl, "img/interstellar_ft512.jpg");
texCube.setLF(gl, "img/interstellar_lf512.jpg");
texCube.setRT(gl, "img/interstellar_rt512.jpg");

var qw = 768, qh = 512;
var texCanvas = new WGLTexture(gl, qw, qh, false);
var texBlendA = new WGLTexture(gl, qw, qh, true);
var texBlendB = new WGLTexture(gl, qw, qh, true);

//should be pegged to visible canvas size and not render size
var qbw = 384, qbh = 256;
var texMiniA = new WGLTexture(gl, qbw, qbh, true);
var texMiniB = new WGLTexture(gl, qbw, qbh, true);

gl.canvas.width = qw;
gl.canvas.height = qh;

quad = new WGLQuad(gl);

//create camera
var camera = new Fig.Camera();
var camDistance = 7.0;
camera.position = [0,0,camDistance];
camera.setFOVy(45);
camera.setAspect(qw/qh);

var phi = 1.5708;
var theta = 0.0;
var lookTarget = [0,0,0];

var autorotate = true;
var animate = false
var invalid = true;
var RADIANS_PER_PIXEL = 0.007;
$canvas.addMouseDragHandler({
  onDragStart: function(p, b){
    autorotate = false;
  },
  onDragEnd: function(p, b){
    autorotate = true;
  },
  
  onDrag: function(p, dp, b){
    if(b != 1){ return; }
    
    theta += RADIANS_PER_PIXEL*dp[0];
    phi += RADIANS_PER_PIXEL*dp[1];
  },
  
  onClick: function(p, b){
    animate = !animate;
  }
}, 8);


//various options
var light = new Light();

var skyboxFactor = 1.0
var material = {
  ambientCoefficient: 0.4,
  diffuseCoefficient: 0.6,
  specularCoefficient: 1.0,
  specularExponent: 60.0,
  reflectionFactor: 0.4
};

var optsPP = {
  enableSSAO: true,
  blurMin: 0.3, blurMax: 1.0,
  fogMin: 0.0, fogMax: 0.6,
  fogColor: [0.3, 0.0, 0.0],
  adjustBrightness: 0.1,
  adjustContrast: 2.0,
  adjustSaturation: 0.8
};

var isoval = 0.5;
var debugmode = "none";
var rendermode = {
  modeDebugSSAO: false,
  modeDebug: false, 
  modeInterpolate: true
};


//main loop--after text resources have loaded
var ready = function(shaders){

  //proprocess GLSL
  var includes = {
    "lighting": shaders["inclLighting.txt"],
    "packFloat": shaders["inclPackFloat.txt"],
    "packColor": shaders["inclPackColor.txt"],
    "dataOctree": shaders["inclDataOctree.txt"],
    "dataVoxels": shaders["inclDataVoxels.txt"],
    "dimensions": shaders["inclDataDims.txt"]
  };
  
  var preprocessList = [
    "quadFragDebugData.txt",
    "quadFragPP.txt", "quadFragSSAO.txt",
    "quadFragData.txt", "quadFragNormals.txt",
    "quadFragOctree.txt", "quadFragOctreeSub.txt", 
    "quadFragRender.txt", 
    "inclDataVoxels.txt", "inclDataOctree.txt"
  ];

  var shadersIncl = {};  
  for(var i=0; i<preprocessList.length; i++){
    var key = preprocessList[i];
    shadersIncl[key] = preprocessGLSLES(shaders[key], includes);    
  }
    
  //create shaders   
  var shaderQuadCopy = new WGLShaderQuadCopy(gl, "copy",
    shaders["quadVertCopy.txt"],
    shaders["quadFragCopy.txt"]);
  var shaderQuadDebugData = new WGLShaderQuadCopy(gl, "debug data",
    shaders["quadVertCopy.txt"],
    shadersIncl["quadFragDebugData.txt"]);
  var shaderQuadDebugOctree = new WGLShaderQuadCopy(gl, "debug octree",
    shaders["quadVertCopy.txt"],
    shaders["quadFragDebugOctree.txt"]);
  var shaderQuadBlurV = new WGLShaderQuadBlur(gl, "blur V",
    shaders["quadVertCopy.txt"],
    shaders["quadFragBlurV.txt"]);
  var shaderQuadBlurH = new WGLShaderQuadBlur(gl, "blur H",
    shaders["quadVertCopy.txt"],
    shaders["quadFragBlurH.txt"]);
  var shaderQuadPP = new WGLShaderQuadPP(gl, "post-processing",
    shaders["quadVertCopy.txt"],
    shadersIncl["quadFragPP.txt"]);
  var shaderQuadDOF = new WGLShaderQuadDOF(gl, "DOF",
    shaders["quadVertCopy.txt"],
    shaders["quadFragDOF.txt"]);
  var shaderQuadSSAO = new WGLShaderQuadSSAO(gl, "SSAO",
    shaders["quadVertCopy.txt"],
    shadersIncl["quadFragSSAO.txt"]);
    
  var shaderQuadData = new WGLShaderQuadData(gl, "data",
    shaders["quadVertCopy.txt"],
    shadersIncl["quadFragData.txt"]);
  var shaderQuadNormals = new WGLShaderQuadNormals(gl, "normals",
    shaders["quadVertCopy.txt"],
    shadersIncl["quadFragNormals.txt"]);
  var shaderQuadOctree = new WGLShaderQuadOctree(gl, "octree (initial)", 
    shaders["quadVertCopy.txt"],
    shadersIncl["quadFragOctree.txt"]);
  var shaderQuadOctreeSub = new WGLShaderQuadOctreeSub(gl, "octree (sub)",
    shaders["quadVertCopy.txt"],
    shadersIncl["quadFragOctreeSub.txt"]);
  
  var shaderQuadRender = new WGLShaderQuadRender(gl, "render",
    shaders["quadVertRender.txt"],
    shadersIncl["quadFragRender.txt"]);
  
  //copy data texture to canvas
  var debugQuad = function(texSource, shader){
    shader.use(gl);
    shader.prepareVertexAttribs(gl, quad);
    
    texSource.bind(gl, WGLShaderQuadCopy.COLOR_TEXTURE_UNIT);
    quad.draw(gl);
    texSource.unbind(gl, WGLShaderQuadCopy.COLOR_TEXTURE_UNIT);
    
    shader.unuse(gl);
  };
  
  var blurQuad = function(texSource){
    //texSource -> texMiniA using copy
    if(texSource != texMiniA){
      texMiniA.bindWrite(gl);
      
      shaderQuadCopy.use(gl);
      shaderQuadCopy.prepareVertexAttribs(gl, quad);
      
      texSource.bind(gl, WGLShaderQuadCopy.COLOR_TEXTURE_UNIT);
      quad.draw(gl);
      texSource.unbind(gl, WGLShaderQuadCopy.COLOR_TEXTURE_UNIT);
      
      shaderQuadCopy.unuse(gl);
      texMiniA.unbindWrite(gl);
    }
    
    //texMiniA -> texMiniB using BlurV
    texMiniB.bindWrite(gl);
    
    shaderQuadBlurV.use(gl);
    shaderQuadBlurV.prepareVertexAttribs(gl, quad);
    
    shaderQuadBlurV.setSourceTextureSize(gl, texMiniA.width, texMiniA.height);
    texMiniA.bind(gl, WGLShaderQuadBlur.COLOR_TEXTURE_UNIT);
    quad.draw(gl);
    texMiniA.unbind(gl, WGLShaderQuadBlur.COLOR_TEXTURE_UNIT);
    
    shaderQuadBlurV.unuse(gl);
    texMiniB.unbindWrite(gl);
    
    //texMiniB -> texMiniA using blurH
    texMiniA.bindWrite(gl);
    
    shaderQuadBlurH.use(gl);
    shaderQuadBlurH.prepareVertexAttribs(gl, quad);
    
    shaderQuadBlurH.setSourceTextureSize(gl, texMiniB.width, texMiniB.height);
    texMiniB.bind(gl, WGLShaderQuadBlur.COLOR_TEXTURE_UNIT);
    quad.draw(gl);
    texMiniB.unbind(gl, WGLShaderQuadBlur.COLOR_TEXTURE_UNIT);
    
    shaderQuadBlurH.unuse(gl);
    texMiniA.unbindWrite(gl);
  };
  
  //write voxel data
  var drawQuadData = function(TIME){
    var shader = shaderQuadData;
    
    shader.use(gl);
    shader.prepareVertexAttribs(gl, quad);
    shader.setTIME(gl, TIME);
    
    quad.draw(gl);
    
    shader.unuse(gl);
  };
  
  //write normal vectors
  var drawQuadNormals = function(texData){
    var shader = shaderQuadNormals;
    
    shader.use(gl);
    shader.prepareVertexAttribs(gl, quad);
    texData.bind(gl, WGLShaderQuadNormals.VOXEL_TEXTURE_UNIT);
    
    shader.setIsoVal(gl, isoval);
    
    quad.draw(gl);
    
    texData.unbind(gl, WGLShaderQuadNormals.VOXEL_TEXTURE_UNIT);
    shader.unuse(gl);
  };
  
  //write octree data
  var drawQuadOctreeInitial = function(texData){
    var shader = shaderQuadOctree;
    
    gl.clearColor(0,0,0,0);
    gl.clear(gl.COLOR_BUFFER_BIT);
    
    shader.use(gl);
    shader.prepareVertexAttribs(gl, quad);
    texData.bind(gl, WGLShaderQuadOctree.VOXEL_TEXTURE_UNIT);
    
    shader.setIsoVal(gl, isoval);
    
    quad.draw(gl);
    
    texData.unbind(gl, WGLShaderQuadOctree.VOXEL_TEXTURE_UNIT);
    shader.unuse(gl);
  };
  
  //write octree levels
  var drawQuadOctreeSub = function(texOctreeSrc, level){
    var shader = shaderQuadOctreeSub;
    
    gl.clearColor(0,0,0,0);
    gl.clear(gl.COLOR_BUFFER_BIT);
    
    shader.use(gl);
    shader.prepareVertexAttribs(gl, quad);
    texOctreeSrc.bind(gl, WGLShaderQuadOctreeSub.OCTREE_TEXTURE_UNIT);
    
    shader.setLevel(gl, level);
    
    quad.draw(gl);
    
    texOctreeSrc.unbind(gl, WGLShaderQuadOctreeSub.OCTREE_TEXTURE_UNIT);
    shader.unuse(gl);
  };
  
  //render voxel data to canvas
  var drawQuadRender = function(texData, texOctree, texEnv){
  
    texCanvas.bindWrite(gl);
    
    gl.clearColor(0,0,0,0);
    gl.clear(gl.COLOR_BUFFER_BIT);
    
    shaderQuadRender.use(gl);
    shaderQuadRender.prepareVertexAttribs(gl, quad);
    
    texData.bind(gl, WGLShaderQuadRender.VOXEL_TEXTURE_UNIT);
    texOctree.bind(gl, WGLShaderQuadRender.OCTREE_TEXTURE_UNIT);
    texEnv.bind(gl, WGLShaderQuadRender.ENV_TEXTURE_UNIT);
    
    shaderQuadRender.setTargetTextureSize(gl, texCanvas.width, texCanvas.height);
    shaderQuadRender.setCameraUniforms(gl, camera);
    shaderQuadRender.setLightUniforms(gl, light);
    shaderQuadRender.setMaterialUniforms(gl, material);
    shaderQuadRender.setMaterialReflection(gl, material.reflectionFactor);
    shaderQuadRender.setSkyboxFactor(gl, skyboxFactor);
    shaderQuadRender.setRenderMode(gl, rendermode);
    shaderQuadRender.setIsoVal(gl, isoval);
    
    quad.draw(gl);
    
    texEnv.unbind(gl, WGLShaderQuadRender.ENV_TEXTURE_UNIT);
    texOctree.unbind(gl, WGLShaderQuadRender.OCTREE_TEXTURE_UNIT);
    texData.unbind(gl, WGLShaderQuadRender.VOXEL_TEXTURE_UNIT);
    
    shaderQuadRender.unuse(gl);
    
    texCanvas.unbindWrite(gl);

    //SSAO texCanvas -> texBlendA
    //---------------------------
    if(optsPP.enableSSAO){
      texBlendA.bindWrite(gl);
      
      shaderQuadSSAO.use(gl);
      shaderQuadSSAO.prepareVertexAttribs(gl, quad);
      texCanvas.bind(gl, WGLShaderQuadSSAO.COLOR_TEXTURE_UNIT);
      
      shaderQuadSSAO.setSourceTextureSize(gl, texCanvas.width, texCanvas.height);
      
      quad.draw(gl);
      
      texCanvas.unbind(gl, WGLShaderQuadSSAO.COLOR_TEXTURE_UNIT);
      shaderQuadSSAO.unuse(gl);
      
      texBlendA.unbindWrite(gl);
    }else{
      texBlendA.bindWrite(gl);
      gl.clearColor(0,0,0,0);
      gl.clear(gl.COLOR_BUFFER_BIT);
      texBlendA.unbindWrite(gl);
    }
    //---------------------------
    
    //blur texBlendA -> texMiniA
    blurQuad(texBlendA);
    
    //texCanvas with SSAO, fog, color adjust -> texBlendB
    //---------------------------
    texBlendB.bindWrite(gl);
    
    shaderQuadPP.use(gl);
    shaderQuadPP.prepareVertexAttribs(gl, quad);
    texCanvas.bind(gl, WGLShaderQuadPP.COLOR_TEXTURE_UNIT);
    texMiniA.bind(gl, WGLShaderQuadPP.SSAO_TEXTURE_UNIT);
    
    shaderQuadPP.setRenderMode(gl, rendermode);
    shaderQuadPP.setFogUniforms(gl, optsPP);
    shaderQuadPP.setColorAdjustUniforms(gl, optsPP);
    
    quad.draw(gl);
    
    texMiniA.unbind(gl, WGLShaderQuadPP.SSAO_TEXTURE_UNIT);
    texCanvas.unbind(gl, WGLShaderQuadPP.COLOR_TEXTURE_UNIT);
    shaderQuadPP.unuse(gl);
    
    texBlendB.unbindWrite(gl);
    //---------------------------
    
    //blur texBlendB -> texMiniA
    blurQuad(texBlendB);
    for(var i=0; i<1; i++){
      blurQuad(texMiniA);
    }
    
    //texBlendB + texMiniA (DOF) -> texBlendA
    //---------------------------
    texBlendA.bindWrite(gl);
    
    shaderQuadDOF.use(gl);
    shaderQuadDOF.prepareVertexAttribs(gl, quad);
    texBlendB.bind(gl, WGLShaderQuadDOF.COLOR_TEXTURE_UNIT);
    texMiniA.bind(gl, WGLShaderQuadDOF.BLUR_TEXTURE_UNIT);
    
    shaderQuadDOF.setBlurUniforms(gl, optsPP);
    
    quad.draw(gl);
    
    texMiniA.unbind(gl, WGLShaderQuadDOF.BLUR_TEXTURE_UNIT);    
    texBlendB.unbind(gl, WGLShaderQuadDOF.COLOR_TEXTURE_UNIT);
    shaderQuadDOF.unuse(gl);
    
    texBlendA.unbindWrite(gl);
    //---------------------------
  };
  
  //render data textures  
  var prepareData = function(time){
    
    //write voxel data to texData
    texData.bindWrite(gl);
    drawQuadData(time);
    texData.unbindWrite(gl);
  };
  
  var prepareOctree = function(){
    //write finest octree level (level 0: [64]^3)
    //note: voxel data is for voxel *corners*, so actually only [63]^3 voxels
    //but, write with 64 anyway so it is power of 2; 64th index always should be 0
    texOctreeA.bindWrite(gl);
    drawQuadOctreeInitial(texData);
    texOctreeA.unbindWrite(gl);
    
    //ping-pong octree levels, writing with quadFracOctreeSub
    //see WGLShaderQuadOctreeSub.setLevel doc for description
    for(var i=1; i<7; i++){
      //swap the octrees: always read from B, write to A
      var texTemp = texOctreeB;
      texOctreeB = texOctreeA;
      texOctreeA = texTemp;
      
      //write another level to A
      texOctreeA.bindWrite(gl);
      drawQuadOctreeSub(texOctreeB, i);
      texOctreeA.unbindWrite(gl);
    }
    
    //write normal data
    gl.enable(gl.BLEND);
    gl.blendFunc(gl.ONE, gl.ONE);
    
    texOctreeA.bindWrite(gl);
    drawQuadNormals(texData);
    texOctreeA.unbindWrite(gl);
    
    gl.disable(gl.BLEND);
  };
    
  //var rotationSpeed = //rads/sec
  var TIME = 0;
  var tspeedFunc = 4.0*Math.PI/30.0;
  var tspeedRot = 2.0*Math.PI/30.0;
  var curtime = Date.now()*0.001;
  
  var framecount = 0;
  var showfpsDelay = 4.0; //sec
  var showfpsNext = showfpsDelay;
  
  (function loop(){
    var newtime = Date.now()*0.001;
    var dt = newtime - curtime;
    curtime = newtime;
    
    framecount++;
    showfpsNext -= dt;
    if(showfpsNext < 0){ 
      console.log("FPS: " + (framecount / showfpsDelay));
      showfpsNext += showfpsDelay;
      framecount = 0;
    }
    
    //update camera and light
    if(autorotate){
      theta += tspeedRot*dt;
    }
    
    camera.position[0] = -camDistance * Math.sin(phi) * Math.cos(theta);
    camera.position[2] = -camDistance * Math.sin(phi) * Math.sin(theta);
    camera.position[1] = -camDistance * Math.cos(phi);
    camera.lookAt(lookTarget);
    
    camera.getRelativePosition(light.camera.position, [2.0,2.0,-2.0]);
    
    //drawing
    //-----------------------
    fbo.bind(gl);
    gl.disable(gl.DEPTH_TEST);
    
    //prepare data and octree
    
    if(animate){ 
      TIME += tspeedFunc*dt;
      prepareData(TIME % 10000);
      invalid = true;
    }
    if(invalid){
      prepareOctree();
      invalid = false;
    }
    //render to texBlendA
    drawQuadRender(texData, texOctreeA, texCube);    
    
    fbo.unbind(gl);
    
    //draw to canvas
    gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
    
    switch(debugmode){
    case "data": debugQuad(texData, shaderQuadDebugData); break;
    case "octree": debugQuad(texOctreeA, shaderQuadDebugOctree); break;
    case "march": debugQuad(texCanvas, shaderQuadCopy); break;
    default: debugQuad(texBlendA, shaderQuadCopy);
    }
    //-----------------------
        
    //next iteration
    requestAnimationFrame(loop);
  })();
  
};
ready(StaticTextRC);


$(document).ready(function(){  
  $("#tabs").tabs();
  
  //rendering
  //--------------------------
  $("#render-mode").selectmenu({
    change: function( event, data ){
      var val = data.item.value;
      debugmode = val;
      rendermode.modeDebugSSAO = (val == "ssao");
      rendermode.modeDebug = (val == "march");
    }
  });
  
  $("#render-interp").selectmenu({
    change: function( event, data ){
      var val = parseInt(data.item.value);
      rendermode.modeInterpolate = (val == 1);
    }
  });
  
  var doneLoadingData = function(){ invalid = true; };
  var onDataClick = function(url){
    return function(e){
      e.preventDefault();
      animate = false;
      texData.setImage(gl, url, doneLoadingData);
    };
  };
  //256^3 data sets
  $("#data-skull").button().click(onDataClick("img/dataSkull85.jpg"));
  $("#data-aneurism").button().click(onDataClick("img/dataAneurism.png"));
  $("#data-bonsai").button().click(onDataClick("img/dataBonsai90.jpg"));
  
  var setIsoVal = function(){ 
    $("#iso-slider-val").text(isoval); 
  };
  setIsoVal();
  $("#iso-slider").slider({
    min: 0.0, max: 1.0, step: 0.02,
    value: isoval,
    slide: function( event, ui ) {
      isoval = ui.value;
      invalid = true;
      setIsoVal();
    }
  });
  
  var setCamDistanceVal = function(){ 
    $("#cam-distance-val").text(camDistance); 
  };
  setCamDistanceVal();
  $("#cam-distance").slider({
    min: 5.0, max: 10.0, step: 0.1,
    value: camDistance,
    slide: function( event, ui ) {
      camDistance = ui.value;
      setCamDistanceVal();
    }
  });
  
  //materials/textures
  //--------------------------
  var setCoeffAmbientVal = function(){ 
    $("#coeff-ambient-val").text(material.ambientCoefficient); 
  };
  setCoeffAmbientVal();
  $("#coeff-ambient").slider({
    min: 0.0, max: 1.0, step: 0.1,
    value: material.ambientCoefficient,
    slide: function( event, ui ) {
      material.ambientCoefficient = ui.value;
      setCoeffAmbientVal();
    }
  });
  
  var setCoeffDiffuseVal = function(){ 
    $("#coeff-diffuse-val").text(material.diffuseCoefficient); 
  };
  setCoeffDiffuseVal();
  $("#coeff-diffuse").slider({
    min: 0.0, max: 1.0, step: 0.1,
    value: material.diffuseCoefficient,
    slide: function( event, ui ) {
      material.diffuseCoefficient = ui.value;
      setCoeffDiffuseVal();
    }
  });
  
  var setCoeffSpecularVal = function(){ 
    $("#coeff-specular-val").text(material.specularCoefficient); 
  };
  setCoeffSpecularVal();
  $("#coeff-specular").slider({
    min: 0.0, max: 1.0, step: 0.1,
    value: material.specularCoefficient,
    slide: function( event, ui ) {
      material.specularCoefficient = ui.value;
      setCoeffSpecularVal();
    }
  });
  
  var setSpecularExponentVal = function(){ 
    $("#spec-exp-val").text(material.specularExponent);
  };
  setSpecularExponentVal();
  $("#specular-exp").slider({
    min: 0.0, max: 100.0, step: 1.0,
    value: material.specularExponent,
    slide: function( event, ui ) {
      material.specularExponent = ui.value;
      setSpecularExponentVal();
    }
  });

  var setReflectionFactorVal = function(){ 
    $("#refl-factor-val").text(material.reflectionFactor);
  };
  setReflectionFactorVal();
  $("#reflection-factor").slider({
    min: 0.0, max: 1.0, step: 0.1,
    value: material.reflectionFactor,
    slide: function( event, ui ) {
      material.reflectionFactor = ui.value;
      setReflectionFactorVal();
    }
  });
  
  var setSkyboxFactorVal = function(){ 
    $("#skybox-factor-val").text(skyboxFactor);
  };
  setSkyboxFactorVal();
  $("#skybox-factor").slider({
    min: 0.0, max: 1.0, step: 0.1,
    value: skyboxFactor,
    slide: function( event, ui ) {
      skyboxFactor = ui.value;
      setSkyboxFactorVal();
    }
  });
  
  //post-processing
  //--------------------------
  var $ssaoEnabled = $("#ssao-enabled");
  $ssaoEnabled[0].checked = optsPP.enableSSAO;
  $ssaoEnabled.change(function(){
    optsPP.enableSSAO = $ssaoEnabled[0].checked;
  });
  $ssaoEnabled.button();
  
  var setBlurRangeVal = function(){ 
    $("#blur-val").text(optsPP.blurMin + "-" + optsPP.blurMax); 
  };
  setBlurRangeVal();
  $("#blur-range").slider({
    range: true,
    min: 0.0, max: 1.0, step: 0.1,
    values: [ optsPP.blurMin, optsPP.blurMax ],
    slide: function( event, ui ) {
      optsPP.blurMin = ui.values[0];
      optsPP.blurMax = ui.values[1];
      setBlurRangeVal();
    }
  });
  
  var setFogRangeVal = function(){ 
    $("#fog-val").text(optsPP.fogMin + "-" + optsPP.fogMax); 
  };
  setFogRangeVal();
  $("#fog-range").slider({
    range: true,
    min: 0.0, max: 1.0, step: 0.1,
    values: [ optsPP.fogMin, optsPP.fogMax ],
    slide: function( event, ui ) {
      optsPP.fogMin = ui.values[0];
      optsPP.fogMax = ui.values[1];
      setFogRangeVal();
    }
  });
  
  var setFogRVal = function(){ 
    $("#fog-r-val").text(optsPP.fogColor[0]); 
  };
  setFogRVal();
  $("#fog-r").css("background", "#FF0000");
  $("#fog-r").slider({
    min: 0.0, max: 1.0, step: 0.1,
    value: optsPP.fogColor[0],
    slide: function( event, ui ) {
      optsPP.fogColor[0] = ui.value;
      setFogRVal();
    }
  });
  
  var setFogGVal = function(){ 
    $("#fog-g-val").text(optsPP.fogColor[1]); 
  };
  setFogGVal();
  $("#fog-g").css("background", "#00FF00");
  $("#fog-g").slider({
    min: 0.0, max: 1.0, step: 0.1,
    value: optsPP.fogColor[1],
    slide: function( event, ui ) {
      optsPP.fogColor[1] = ui.value;
      setFogGVal();
    }
  });
  
  var setFogBVal = function(){ 
    $("#fog-b-val").text(optsPP.fogColor[2]); 
  };
  setFogBVal();
  $("#fog-b").css("background", "#0000FF");
  $("#fog-b").slider({
    min: 0.0, max: 1.0, step: 0.1,
    value: optsPP.fogColor[2],
    slide: function( event, ui ) {
      optsPP.fogColor[2] = ui.value;
      setFogBVal();
    }
  });
  
  var setAdjustBVal = function(){ 
    $("#adjust-b-val").text(optsPP.adjustBrightness); 
  };
  setAdjustBVal();
  $("#adjust-brightness").slider({
    min: -1.0, max: 1.0, step: 0.1,
    value: optsPP.adjustBrightness,
    slide: function( event, ui ) {
      optsPP.adjustBrightness = ui.value;
      setAdjustBVal();
    }
  });
  
  var setAdjustCVal = function(){ 
    $("#adjust-c-val").text(optsPP.adjustContrast); 
  };
  setAdjustCVal();
  $("#adjust-contrast").slider({
    min: -3.0, max: 3.0, step: 0.2,
    value: optsPP.adjustContrast,
    slide: function( event, ui ) {
      optsPP.adjustContrast = ui.value;
      setAdjustCVal();
    }
  });
  
  var setAdjustSVal = function(){ 
    $("#adjust-s-val").text(optsPP.adjustSaturation); 
  };
  setAdjustSVal();
  $("#adjust-saturation").slider({
    min: -3.0, max: 3.0, step: 0.2, 
    value: optsPP.adjustSaturation,
    slide: function( event, ui ) {
      optsPP.adjustSaturation = ui.value;
      setAdjustSVal();
    }
  });
  
  $("#canvas").resizable({ aspectRatio: qw/qh });
});
